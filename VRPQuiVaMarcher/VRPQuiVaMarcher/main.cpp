#include "fonction.h"

int main()
{
	graphe_t monGraphe;
	solution_t maSolution;
	monGraphe = lecture_fichier("HVRP_DLP_01.txt");
	//afficher_graphe(monGraphe);
	initialiser_solution(&monGraphe, &maSolution);
	liberer_solution(monGraphe, &maSolution);
	//initialiser_solution(&monGraphe, &maSolution);
	generer_tours_geants(monGraphe, &maSolution);
	afficher_tour_geant(monGraphe, maSolution);
	split(monGraphe, &maSolution);
	liberer_graphe(&monGraphe);
}