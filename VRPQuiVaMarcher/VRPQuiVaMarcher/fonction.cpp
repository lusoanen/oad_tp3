#include "fonction.h"

const int infini = 9999999;


graphe_t lecture_fichier(string nomFichier)
{
    graphe_t graphe;

    int nbClients,
        nbNoeuds,
        capaciteVehicules;

    fstream monFlux(nomFichier, ios::in);

    monFlux >> nbClients;
    monFlux >> capaciteVehicules;

    graphe.nbClients = nbClients;
    graphe.capaciteVehicules = capaciteVehicules;
    graphe.nbNoeuds = nbClients + 1;


    graphe.matriceCout = (int**)malloc(graphe.nbNoeuds * sizeof(int*));
    graphe.commandesClient = (int*)malloc(graphe.nbClients * sizeof(int));

    

    for (int i = 0; i < graphe.nbNoeuds; ++i)
    {
        graphe.matriceCout[i] = (int*)malloc(graphe.nbNoeuds * sizeof(int));
    }

    for (int i = 0; i < graphe.nbNoeuds; ++i)
    {
        for (int j = 0; j < graphe.nbNoeuds; ++j)
        {
            monFlux >> graphe.matriceCout[i][j];
        }
    }

    for (int i = 0; i < graphe.nbClients; ++i)
    {
        monFlux >> graphe.commandesClient[i];
    }

    return graphe;
}

void afficher_graphe(graphe_t graphe)
{
    cout << "Il y a " << graphe.nbClients << " clients à livrer." << endl << endl;

    cout << "Les véhicules ont une capacité de " << graphe.capaciteVehicules << "." << endl << endl;

    cout << "Matrice des coûts de transport :" << endl << endl;

    for (int i = 0; i < graphe.nbNoeuds; ++i)
    {
        for (int j = 0; j < graphe.nbNoeuds; ++j)
        {
            cout << "|" << graphe.matriceCout[i][j];
        }
        cout << "|" << endl;
    }
    cout << endl;

    cout << "Matrice des commandes de client :" << endl << endl;

    for (int i = 0; i < graphe.nbClients; ++i)
    {
        cout << "|" << graphe.commandesClient[i];
    }
    cout << "|" << endl << endl;
}

void initialiser_solution(graphe_t* graphe, solution_t* solution)
{
    solution->poidsTours = (int*)malloc(graphe->nbClients * sizeof(int));
    solution->coutTours = (int*)malloc(graphe->nbClients * sizeof(int));
    solution->matriceCoutVoisin = (int*)malloc(graphe->nbClients * sizeof(int));
    solution->pere = (int*)malloc(graphe->nbClients * sizeof(int));

    for (int i = 0; i< graphe->nbClients; ++i)
    {
        solution->poidsTours[i] = 0;
        solution->coutTours[i] = 0;
        solution->pere[i] = -1;
    }
}


void split(graphe_t graphe, solution_t* solution)
{
    int j = -1;

    solution->matriceCoutVoisin[0] = 0;
    for (int i = 1; i < graphe.nbClients; ++i)
    {
        solution->matriceCoutVoisin[i] = -infini;
    }

    for (int i = 0; i < graphe.nbClients; ++i)
    {
        j = i + 1;
        do
        {
            if (j == i + 1)
            {
                solution->poidsTours[i] = graphe.matriceCout[0][j]+ graphe.matriceCout[j][0];
                solution->coutTours[i] = graphe.commandesClient[j];
            }
            else
            {
                solution->poidsTours[i] = solution->poidsTours[i] - graphe.matriceCout[j-1][0] + graphe.matriceCout[j - 1][j] + graphe.matriceCout[j][0];
                solution->coutTours[i] += graphe.commandesClient[j];

                if (solution->matriceCoutVoisin[i] + solution->coutTours[i] < solution->matriceCoutVoisin[j])
                {
                    solution->matriceCoutVoisin[j] = solution->matriceCoutVoisin[i] + solution->coutTours[i];
                    solution->pere[j] = i;
                }
            }

            j++;
        } while ((j<=graphe.nbClients) && (solution->coutTours[i] + graphe.commandesClient[j] <= graphe.capaciteVehicules));
    }
}

void liberer_solution(graphe_t graphe, solution_t* solution)
{
    free(solution->poidsTours);
    free(solution->coutTours);
    free(solution->matriceCoutVoisin);
    free(solution->pere);
}

void generer_tours_geants(graphe_t graphe, solution_t* solution)
{
    int tg[NBCLIENTSMAX + 2];
	int estUtilise[NBCLIENTSMAX];
	int nbNonRange = graphe.nbClients;

    for (int i = 0; i < graphe.nbClients; ++i)
    {
        estUtilise[i] = i +1;
    }

    srand(time(NULL));
	int indice;
	int j = 1;

    while (nbNonRange > 0)
    {
		indice = rand() % nbNonRange;
		tg[j] = estUtilise[indice];
		estUtilise[indice] = estUtilise[nbNonRange - 1];
		nbNonRange--;
		j++;
    }

	tg[0] = 0;
	tg[graphe.nbClients + 1] = 0;

	solution->tg = tg;
}

/*
int* recherchePlusProche(graphe_t graphe, int* top) {

}
*/

void generer_tour_geant_voisin_random(graphe_t graphe, solution_t* solution) {
	int tg[NBCLIENTSMAX + 2];
	int estUtilise[NBCLIENTSMAX];
	int nbNonRange = graphe.nbClients;

	for (int i = 0; i < graphe.nbClients; ++i)
	{
		estUtilise[i] = i + 1;
	}

	srand(time(NULL));
	int indice;
	int top5[5];

	while (nbNonRange > 0)
	{
		//recherchePlusProche(graphe, top5);
	}
}

void afficher_tour_geant(graphe_t graphe, solution_t solution)
{
    int i = 0,
        j = 0;

	for (i; i < graphe.nbClients + 2; i++) {
		cout << solution.tg[i] << "; ";
	}
   

}

void liberer_graphe(graphe_t* graphe)
{
    for (int i = 0; i < graphe->nbNoeuds; ++i)
    {
        free(graphe->matriceCout[i]);
    }

    free(graphe->matriceCout);
    free(graphe->commandesClient);
}