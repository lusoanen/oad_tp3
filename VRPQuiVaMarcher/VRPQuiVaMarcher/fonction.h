#pragma once
#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
using namespace std;
const int NBCLIENTSMAX = 1000;

typedef struct graph
{
	int nbClients;
	int nbNoeuds;
	int capaciteVehicules;
	int** matriceCout;
	int* commandesClient;
}graphe_t;

typedef struct solution
{
	int* tg;
	int* poidsTours;
	int* coutTours;
	int* matriceCoutVoisin;
	int* pere;
}solution_t;

graphe_t lecture_fichier(string nomFichier);
void afficher_graphe(graphe_t graphe);
void initialiser_solution(graphe_t* graphe, solution_t* solution);
void split(graphe_t graphe, solution_t* solution);
void liberer_solution(graphe_t graphe, solution_t* solution);
void generer_tours_geants(graphe_t graphe, solution_t* solution);
void afficher_tour_geant(graphe_t graphe, solution_t solution);
void liberer_graphe(graphe_t* graphe);